from diagram.node import Node
from diagram.memoryless_seq import MemoryLessSeq


class Arc(object):

    def __init__(self, node_1: Node, node_2: Node):

        self.node_1 = node_1    # node 1
        self.node_2 = node_2    # node 2

    def __repr__(self):
        return 'Arc(' + repr(self.node_1) + ', ' + repr(self.node_2) + ')'

    def __str__(self):
        return repr(self) + '\n Values: ' + str(self.node_1.get_m()) + ' -> ' + str(self.node_2.get_m())

    def __hash__(self):
        return hash(str(self.node_1)+str(self.node_2))

    def get_node_1(self):
        return self.node_1

    def get_node_2(self):
        return self.node_2

    def get_value(self, mls: MemoryLessSeq):

        """ Return the value of the arc """

        k = self.node_1.get_k()

        return mls.value(k, self.node_1.get_m(), self.node_2.get_m())

    def get_card_value(self, mls: MemoryLessSeq):

        return len(self.get_value(mls))







