import unittest

from diagram.memoryless_seq import Value

from mathobjects.partition import MemoryLessSeqPartition, DiagramPartition
from mathobjects.permutation import MemoryLessSeqPermutation, DiagramPermutation


class TestPartition(unittest.TestCase):

    def setUp(self):
        self.mls = MemoryLessSeqPartition(5)

        self.diagram = DiagramPartition(5)
        self.diagram.make_complete()

    def test_print(self):
        print(self.mls)
        print(self.diagram)

    def test_plot(self):
        self.diagram.plot()

    def test_nextValues(self):
        s_values = self.mls.get_next_values(Value(0), 0)
        self.assertSetEqual(s_values, {Value(0), Value(1), Value(2), Value(3), Value(4), Value(5)})
        s_values = self.mls.get_next_values(Value(1), 1)
        self.assertSetEqual(s_values, {Value(1), Value(3), Value(5)})

    def test_randStates(self):
        l_states = self.diagram.rand_states(5)


class TestPermutation(unittest.TestCase):

    def setUp(self):
        self.mls = MemoryLessSeqPermutation(5)
        self.diagram = DiagramPermutation(5)
        self.diagram.make_complete()

    def test_print(self):
        print(self.mls)
        print(self.diagram)

    def test_nextValues(self):
        s_values = self.mls.get_next_values(Value({1, 3}), 0)
        self.assertSetEqual(s_values, {Value({1, 3, 2}), Value({1, 3, 4}), Value({1, 5, 3})})

    def test_plot(self):
        self.diagram.plot()

    def test_cardStates(self):
        self.assertEqual(self.diagram.card_states(), 5*4*3*2)

    def test_randStates(self):
        l_states = self.diagram.rand_states(5)


if __name__ == '__main__':
    unittest.main()
