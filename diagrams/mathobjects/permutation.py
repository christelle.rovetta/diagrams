from diagram.memoryless_seq import Value, MemoryLessSeq
from diagram.diagram import Diagram


class MemoryLessSeqPermutation(MemoryLessSeq):

    """ MemoryLess Sequence for the partitions of k """

    def __init__(self, k: int):

        self.k = k
        self.max_set = set(range(1, k + 1))

        space = {Value({i}) for i in range(1, k + 1)}

        MemoryLessSeq.__init__(self, space, lambda x, y: x + y, set(), "Permutation of size " + str(k))

    def get_max_set(self):
        return self.max_set

    def get_next_values(self, m: Value, i: int = 0, restrict=None):

        """ Returns the set of all m' that are obtain from m in the layer k """

        m_val = m.get_value()
        diff = self.max_set - m_val

        return {Value(m_val.union({e})) for e in diff}

    def f_k(self, s):

        """ Compute f_k """

        return s[-1]

    def value(self, i, m_1: Value, m_2: Value):

        m_3 = m_2 - m_1

        return {m_3}

    def card_value(self, i, m_1: Value, m_2: Value):

        return len(self.value(i, m_1, m_2))


class DiagramPermutation(Diagram):

    def __init__(self, k):
        mls = MemoryLessSeqPermutation(k)
        Diagram.__init__(self, k, mls.get_max_set(), mls)





