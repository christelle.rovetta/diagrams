import inspect


class Value(object):

    def __init__(self, v):
        self.value = v

    def __hash__(self):
        if isinstance(self.value, set):
            return hash(frozenset(self.value))

        return hash(self.value)

    def __eq__(self, other):
        return self.value == other.value

    def __le__(self, other):
        return self.value <= other.value

    def __add__(self, other):
        return Value(self.value + other.value)

    def __sub__(self, other):
        return Value(self.value - other.value)

    def __mod__(self, other):
        return Value(self.value % other.value)

    def __mul__(self, other):
        return Value(self.value * other.value)

    def __str__(self):
        return 'Value(' + str(self.value) + ')'

    def __repr__(self):
        return repr(self.value)

    def __len__(self):
        return 1

    def get_value(self):
        return self.value


class MemoryLessSeq(object):

    """ Memoryless Sequence """

    def __init__(self, s_e: set, o, z, name: str = ''):

        self.space = {Value(e) for e in s_e}  # Default Space
        self.op = o  # Default Function
        self.zero = Value(z)  # Zero G
        self.name = name

        self.d_space = dict()  # Specific Space
        self.d_op = dict()  # Specific operator

    def set_op_k(self, f, k: int):

        """ Set the operator in the layer k """

        self.d_op[k] = f

    def get_op_k(self, k: int):

        """ Return the operator in the layer k """

        if k in self.d_op:
            return self.d_op[k]
        else:
            return self.op

    def set_space_k(self, ek: set, k: int):
        self.d_space[k] = ek

    def get_space_k(self, k: int):

        if k in self.d_space:
            return self.d_space[k]
        else:
            return self.space

    def get_zero(self):

        return self.zero

    def get_next_values(self, m: Value, k: int = 0, restrict=None):

        """ Returns the set of all m' that are obtain from m in the layer k """

        space_k = self.get_space_k(k)
        img = set()
        op = self.get_op_k(k)

        # Find all the m' such that m' = op(m, e)
        for e in space_k:
            img.add(op(m, e))

        # Applies a restriction for img
        if restrict is not None:
            img = {m for m in img if m <= restrict}

        return img

    def f_k(self, s):

        """ Compute f_k """

        k = len(s)
        return self.fk_sum(k, s)

    def fk_sum(self, k, s):

        """
            SM * State -> fk(s)
            Compute fk(s)
        """

        op = self.get_op_k(k)
        m = self.zero

        for i in range(k):
            m = op(m, s[i])

        return m

    def value(self, k, m_1: Value, m_2: Value):

        """
            Returns the value of the arc ((k-1,m1),(k,m2))
            Remark : it could be optimised if op is known
        """

        s_val = set()

        space_k = self.get_space_k(k)
        op_k = self.get_op_k(k)

        for e in space_k:
            if op_k(m_1, e) == m_2:
                s_val.add(e)

        return s_val

    def card_value(self, k, m1, m2):

        """ Returns the size of the arc value """

        v = self.value(k, m1, m2)
        return len(v)

    def __repr__(self):
        aff = 'MemoryLessSeq' + self.name + ':'
        aff += "\n\t Space : " + str(self.space)
        aff += "\n\t Operator: " + inspect.getsource(self.op)
        for k in self.d_op:
            aff += '\t ' + str(k) + "\t" + inspect.getsource(self.d_op[k])

        return aff


