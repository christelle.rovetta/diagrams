from diagram.arc import Arc
from diagram.node import Node


class Layer(set):

    """ Class of layer """

    def __repr__(self):

        l_layer = list(self)
        l_layer.sort(key=lambda arc: arc.get_node_1())
        l_arc = [repr(a) for a in l_layer]

        aff = '\n'.join(l_arc)
        aff = '\nLayer \n{\n' + aff + '\n}\n'

        return aff

    def get_destinations(self):

        """ Returns the set of node destinations in the Layer """

        s_des = {a.get_node_2() for a in self}

        return s_des

    def get_destinations_from_node(self, node_1: Node):

        s_des = [a for a in self if a.get_node_1() == node_1]

        return s_des

    def get_origins(self):
        
        """ Returns the set of all arcs origin """

        s_ori = {a.get_node_1() for a in self}

        return s_ori
