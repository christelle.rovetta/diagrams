import unittest

from diagram.memoryless_seq import Value, MemoryLessSeq
from diagram.state import State
from diagram.node import Node
from diagram.diagram import Diagram


class TestMemoryLessSeq(unittest.TestCase):

    def setUp(self):

        space = set(range(5))   # Space
        self.mls_0 = MemoryLessSeq(space, lambda x, y: x + y, 0)
        self.mls_1 = MemoryLessSeq(space, lambda x, y: x + y, 1)

    def test_print(self):
        print(self.mls_1)

    def test_nextValues(self):
        s_values = self.mls_0.get_next_values(Value(2), 0, Value(4))
        self.assertSetEqual(s_values, {Value(2), Value(3), Value(4)})

    def test_f_k(self):
        state_1 = State([Value(0), Value(2), Value(0), Value(1), Value(0)])

        m_0 = self.mls_0.f_k(state_1)
        self.assertEqual(m_0, Value(3))

        m_1 = self.mls_1.f_k(state_1)
        self.assertEqual(m_1, Value(4))


class TestState(unittest.TestCase):

    def setUp(self):
        space_1 = set(range(5))  # Space
        self.mls_1 = MemoryLessSeq(space_1, lambda x, y: x + y, 0)
        self.state_1 = State([Value(1), Value(0), Value(0), Value(2), Value(0)])
        self.state_2 = State([Value(9)])

    def test_isConform(self):
        test_1 = self.state_1.is_conform(5, Value(3), self.mls_1)
        self.assertTrue(test_1)

    def test_print(self):
        print(self.state_1)

    def test_add(self):
        x = self.state_1 + self.state_2
        print(x)


class TestNode(unittest.TestCase):

    def setUp(self):
        self.node_1 = Node(0, Value(1), 1)
        self.node_1_bis = Node(0, Value(1), 1)
        self.node_2 = Node(0, Value(2), 2)

        space = set(range(5))  # Space
        self.mls = MemoryLessSeq(space, lambda x, y: x + y, 0)

    def test_get_k(self):
        self.assertEqual(self.node_1.get_k(), 0)

    def test_le(self):
        self.assertLessEqual(self.node_1, self.node_2)

    def test_hash(self):

        s_node = set()
        s_node.add(self.node_1)
        s_node.add(self.node_1_bis)

        self.assertEqual(len(s_node), 2)

    def test_getNextValues(self):
        s_value = self.node_1.get_next_values(self.mls, Value(2))
        self.assertSetEqual(s_value, {Value(1), Value(2)})


class TestDiagram(unittest.TestCase):

    def setUp(self):
        self.mls_1 = MemoryLessSeq(set(range(10)), lambda x, y: x + y, 0)
        self.diagram_1 = Diagram(3, 5, self.mls_1)
        self.diagram_1.make_complete()

    def test_len(self):
        self.assertEqual(len(self.diagram_1), 33)

    def test_print(self):
        print(self.diagram_1)

    def test_plot(self):
        self.diagram_1.plot()

    def test_makeClean(self):
        self.diagram_1.make_clean()
        self.diagram_1.plot()

    def test_cardStates(self):
        card = self.diagram_1.card_states()

        # Ensure that the number of States is 21
        self.assertEqual(21, card)  # That is C(3-1, 3-1+5)

    def test_diagramToStates(self):
        # Compute the set of states encoded by the diagram
        s_states = self.diagram_1.diagram_to_states()

        # Ensure that the number of States is 21
        self.assertEqual(21, len(s_states))

    def test_computeWeight(self):
        # Compute the weight of the arcs and the nodes
        w_nodes, w_arcs = self.diagram_1.compute_weight()

        # Ensure that the weight of the origin node is 21
        n_origin = self.diagram_1.get_origin_node()
        self.assertEqual(21, w_nodes[n_origin])

    def test_randStates(self):
        # Sample 5 states in the diagram according the uniform distribution
        l_states = self.diagram_1.rand_states(5)

        # Ensure that the list has a length of 5
        self.assertEqual(5, len(l_states))


if __name__ == '__main__':
    unittest.main()
