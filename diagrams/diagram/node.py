from diagram.memoryless_seq import Value, MemoryLessSeq


class Node(object):

    """ Class of Nodes """

    def __init__(self, k: int, m: Value, tag: int):

        self.k = k          # Layer number
        self.m = m          # Node value
        self.tag = tag      # Node Id (in the layer)

    def __repr__(self):
        return 'Node(' + str(self.k) + ', ' + str(self.tag) + ')'

    def __str__(self):
        return repr(self) + '\n Value: ' + str(self.m)

    def __le__(self, node):
        return (self.k, self.tag) <= (node.k, node.tag)

    def __lt__(self, node):
        return (self.k, self.tag) < (node.k, node.tag)

    def __hash__(self):
        return hash(str(self.k) + str(self.m.get_value()))

    def get_k(self):
        return self.k

    def get_m(self):
        return self.m

    def get_tag(self):
        return self.tag

    def set_tag(self, tag):
        self.tag = tag

    def tag_with_value(self):
        if isinstance(self.m.get_value(), int):
            self.tag = self.m.get_value()
            return True
        return False

    def get_next_values(self, mls: MemoryLessSeq, restrict: Value = None):
        return mls.get_next_values(self.m, self.k, restrict)
