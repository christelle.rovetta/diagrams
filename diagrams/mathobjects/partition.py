from diagram.memoryless_seq import Value, MemoryLessSeq
from diagram.diagram import Diagram


class MemoryLessSeqPartition(MemoryLessSeq):

    """ MemoryLess Sequence for the partitions of k """

    def __init__(self, k: int):

        self.k = k
        space = set(range(k + 1))

        MemoryLessSeq.__init__(self, space, lambda x, y, i: x + i * y, 0, "Partition of " + str(k))

    def get_next_values(self, m: Value, i: int = 0, restrict=None):

        """ Returns the set of all m' that are obtain from m in the layer k """

        img = set()

        j = i + 1
        v_0 = m.get_value()

        for v in range(v_0, self.k + 1, j):
            val = Value(v)
            img.add(val)

        return img

    def f_k(self, s):

        """ Compute f_k """

        l_x = [(i + 1) * s[i] for i in range(self.k)]

        return sum(l_x)

    def value(self, i, m_1: Value, m_2: Value):

        s_val = set()
        j = i + 1
        v_1 = m_1.get_value()
        v_2 = m_2.get_value()

        if (v_2 - v_1) % j == 0:
            v = (v_2 - v_1) / j
            s_val.add(Value(v))

        return s_val

    def card_value(self, i, m_1: Value, m_2: Value):

        j = i + 1
        v_1 = m_1.get_value()
        v_2 = m_2.get_value()

        if (v_2 - v_1) % j == 0:
            return 1

        return 0


class DiagramPartition(Diagram):

    def __init__(self, k):
        mls = MemoryLessSeqPartition(k)
        Diagram.__init__(self, k, k, mls)





