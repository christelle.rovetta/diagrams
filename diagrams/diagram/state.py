from diagram.memoryless_seq import Value, MemoryLessSeq


class State(tuple):

    """ Class of States """

    def is_conform(self, k: int, m: Value, mls):

        """ Returns True if 'self' is a State associated to the Memoryless Sequence mls of size k """

        if len(self) != k:
            return False

        return m == mls.f_k(self)

    def __str__(self):
        return 'State' + repr(self)


class SetStates(object):

    def __init__(self, k, m: Value, mls: MemoryLessSeq):

        """
            list[State]*SM -> SetStates constructor
            Hyp: L is an non empty list which contains of States in Omega(K,M)
        """

        self.k = k
        self.m = m
        self.mls = mls

        self.s_states = set()

    def __len__(self):
        return len(self.s_states)

    def __repr__(self):

        aff = "\n States{"
        for s in self.s_states:
            aff += "\n\t" + str(s)
        aff += '}'

        return aff

    def reset(self):
        self.s_states = set()

    def is_in(self, x):
        return x in self.s_states

    def remove_state(self, x):
        return self.s_states.remove(x)

    def remove_states(self, l_states):
        for x in l_states:
            self.remove_state(x)

    def add_state(self, x):

        """ Add one State """

        if x.is_conform(self.k, self.m, self.mls):
            self.s_states.add(x)
            return True

        return False

    def add_states(self, l_states):

        """ Add a set of State """

        for x in l_states:
            self.add_state(x)




