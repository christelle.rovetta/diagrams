import matplotlib.pyplot as plt
import random

from diagram.memoryless_seq import Value, MemoryLessSeq
from diagram.state import State, SetStates
from diagram.node import Node
from diagram.arc import Arc
from diagram.layer import Layer


class Diagram(object):

    """ Class of diagrams """

    def __init__(self, k: int, m, mls: MemoryLessSeq):

        """ Create an empty diagram """

        self.k = k  # Number of layers
        self.m = Value(m)   # Value of the destination Node
        self.mls = mls  # Memoryless Sequence

        self.origin = Node(0, self.mls.get_zero(), 0)   # Origin node
        self.destination = Node(k, self.m, 0)   # Destination node

        self.l_layer = list()   # List of Layers
        for i in range(self.k):     # Initialise all layer as empty
            self.l_layer.append(Layer())

    def __len__(self):

        """ Returns the size of the diagram that is the number of arcs """

        card = 0
        for i in range(self.k):
            card += len(self.l_layer[i])

        return card

    def __repr__(self):

        aff = '\n Diagram['
        for i in range(self.k):
            aff += repr(self.l_layer[i])
        aff += ']'
        aff += '\n Origin:' + repr(self.origin.get_m())
        aff += '\n Destination:' + repr(self.destination.get_m())

        return aff

    def get_origin_node(self):
        return self.origin

    def get_destination_node(self):
        return self.destination

    def make_complete(self):

        """ Transforms the diagram in a complete diagram """

        s_nodes_1 = {self.origin}

        # Layers constructions
        for i in range(self.k):

            tag = 0
            d_node_2 = dict()

            if i == self.k - 1:
                d_node_2[self.destination.get_m()] = self.destination

            # Create Arcs from node_1
            for node_1 in s_nodes_1:

                next_values_node = node_1.get_next_values(self.mls, self.m)

                for mm in next_values_node:
                    if mm not in d_node_2:
                        node_2 = Node(i+1, mm, tag)
                        d_node_2[mm] = node_2
                        tag += 1
                    else:
                        node_2 = d_node_2[mm]

                    arc = Arc(node_1, node_2)
                    self.l_layer[i].add(arc)

            # Set of destinations Nodes in the Layer
            s_nodes_1 = set(d_node_2.values())

        # Erase arcs that are not on the path of the destination node
        self.make_clean()

        # Tag again the nodes
        if self.origin.tag_with_value():
            self.tag_with_value()

    def make_clean(self):

        """ Erase arcs that are not on the path of the destination node  """

        s_nodes_2 = set()
        s_nodes_2.add(self.destination)

        for i in range(self.k - 1, -1, -1):

            layer = self.l_layer[i]
            layer_ok = {arc for arc in layer if arc.get_node_2() in s_nodes_2}

            if len(layer) == len(layer_ok):
                return

            self.l_layer[i] = Layer()
            for arc in layer_ok:
                self.l_layer[i].add(arc)

            s_nodes_2 = {arc.get_node_1() for arc in layer_ok}

    def tag_with_value(self):

        self.origin.tag_with_value()

        for i in range(self.k):
            layer = self.l_layer[i]
            l_node_2 = [arc.get_node_2() for arc in layer]

            for node in l_node_2:
                node.tag_with_value()

    def plot(self):

        """ Plots the diagram """

        plt.grid()
        plt.scatter(0, self.origin.get_tag(), s=100, c='red', marker='o')  # Plot the origin node

        max_y = 0
        for k in range(0, self.k):

            x = [k, k + 1]
            col = self.l_layer[k]

            # Plot nodes
            s_des = col.get_destinations()  # Set of destination node
            for des in s_des:
                plt.scatter(x[1], des.get_tag(), s=80, c='w', marker='.')
                max_y = max(max_y, des.get_tag())

            # Plot arcs
            for a in col:
                y1 = a.get_node_1().get_tag()
                y2 = a.get_node_2().get_tag()
                y = [y1, y2]
                plt.plot(x, y, c='blue')

        plt.scatter(self.k, self.destination.get_tag(), s=100, c='red', marker='o')

        max_y += 1
        plt.axis([-1, self.k + 1, -1, max_y + 1])

        plt.show()

    def card_states(self):

        """
            Diagram -> int
            Compute |psi(D)|, ie. the number of states encoded by the diagram
        """

        d_count_1 = dict()  # Count the states encoded by an origin node for a given layer
        d_count_1[self.origin] = 1

        # In each Layer
        for i in range(self.k):

            layer = self.l_layer[i]
            d_count_2 = dict()  # Count the states encoded by a destination node for the layer i

            for arc in layer:

                node_1 = arc.get_node_1()
                node_2 = arc.get_node_2()
                card_value = arc.get_card_value(self.mls)

                if node_2 not in d_count_2:
                    d_count_2[node_2] = 0

                d_count_2[node_2] += d_count_1[node_1] * card_value

            d_count_1 = d_count_2

        if self.destination not in d_count_1:
            return 0

        return d_count_1[self.destination]

    def diagram_to_states(self):

        """
            Diagram -> SetStates
            Compute psi(D), ie. the set of states encoded by the diagram
        """

        d_states_1 = dict()  # States encoded by an origin nodes for a given layer
        d_states_1[self.origin] = [[]]     # The Origin node encodes an empty State

        # In each Layer
        for i in range(self.k):

            layer = self.l_layer[i]
            d_states_2 = dict()  # Set of states encoded by a destination node for the layer i

            for arc in layer:

                l_node_1 = d_states_1[arc.get_node_1()]
                node_2 = arc.get_node_2()
                value = arc.get_value(self.mls)

                if node_2 not in d_states_2:
                    d_states_2[node_2] = list()

                for x_i in value:
                    for x_1 in l_node_1:
                        x_2 = x_1 + list([x_i])
                        d_states_2[node_2].append(x_2)

            d_states_1 = d_states_2

        if self.destination not in d_states_1:
            l_states = list()
        else:
            l_states = [State(x) for x in d_states_1[self.destination]]

        s_states = SetStates(self.k, self.m, self.mls)
        s_states.add_states(l_states)

        return s_states

    def compute_weight(self, d_weight_value=dict()):

        d_weight_arc = dict()
        d_weight_node = dict()

        def weight_value(value, j: int):

            """ Returns the weight of a value """

            key = (value, j)

            if key not in d_weight_value:
                if value not in d_weight_value:
                    return 1
                return d_weight_value[value]

            return d_weight_value[key]

        def weight_arc(a: Arc, mls: MemoryLessSeq, j: int):

            """ Returns the weight of an arc """

            if a in d_weight_arc:
                return d_weight_arc[a]

            # Compute the weight of the arc
            weight = 0.0
            for value in a.get_value(mls):
                weight += weight_value(value, j)
            d_weight_arc[a] = weight

            return d_weight_arc[a]

        # Compute the weight of the nodes from layer k-1 to layer 0
        d_weight_node[self.destination] = 1
        for i in range(self.k - 1, -1, -1):

            layer = self.l_layer[i]

            for arc in layer:
                node_1 = arc.get_node_1()
                node_2 = arc.get_node_2()
                w_arc = weight_arc(arc, self.mls, i)

                if node_1 not in d_weight_node:
                    d_weight_node[node_1] = 0.0

                d_weight_node[node_1] += w_arc * d_weight_node[node_2]

        return d_weight_node, d_weight_arc

    def rand_states(self, n: int, d_weight_value=dict()):

        def weight_value(value, j: int):

            """ Returns the weight of a value """

            key = (value, j)

            if key not in d_weight_value:
                if value not in d_weight_value:
                    return 1
                return d_weight_value[value]

            return d_weight_value[key]

        def rand_state():

            node = self.origin
            x = list()
            for i in range(self.k):
                layer = self.l_layer[i]

                # Arc choice
                l_arcs_node = layer.get_destinations_from_node(node)
                w_arcs_node = [w_arcs[a] for a in l_arcs_node]
                arc = random.choices(l_arcs_node, w_arcs_node)[0]

                # Value choice
                l_value_arc = list(arc.get_value(self.mls))
                w_value_arc = [weight_value(v, i) for v in l_value_arc]
                value = random.choices(l_value_arc, w_value_arc)[0]

                x.append(value)

                node = arc.get_node_2()

            return State(x)

        w_nodes, w_arcs = self.compute_weight(d_weight_value)
        l_states = list()
        for nn in range(n):
            state = rand_state()
            l_states.append(state)

        return l_states















